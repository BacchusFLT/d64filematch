## D64 File Match

# Purpose

This program extracts the directory from two different d64 images (c64 disk images) and displays the differences in the files found on both.

The idea is to identify differences in the disk data, but using the c64 file structure as the basis. 

You can do binay comparisons between disk images and get significant differences, even if the files one both and content of the files are the same. This happens if the content was file copied and the files on the destination didn't end up on the very same sector as the source. From a c64 DOS perspective these disks will in this case be the same, even if the binary compare will tell you something else.


# Usage

Call the program with the paramenters being the names of the two d64 files to compare. Verbose output possible, as is printing of a small help text. Then parameter "max" is an option to support disk images with more than 35 sectors.

 FileMatch /s1 /s2 [/v] [/h] [/max] [/?]');


# Remarks

You give two filenames as parameters; the baseline is the first one. Files uniquely on the first one will be skipped and only files available on both will be compared. Files quiniquely on the second will be disregarded. Differences are marked also using the address in the file. If the file load address are different, the address from the first file is used.  

There is an option to send a parameter of number of sectors, and you abort reading of files if the track is above that value. Having said this, the routine to convert track/sector to an absolute index still only supports 35 tracks so if you run above 35 you will still get an error (and why would you want to use less than 35? ;-)


# Project Status

The project provides the expected results in the standard cases. As stated above, images bigger than the 35 tracks still fail, and there is limited robustness against corner cases. File length differences are detected and handled.


(c) 2019, Pontus "Bachcus" Berg
www.bergatrollet.se - www.fairlight.to