program FileMatcher;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils, System.math;

Type

  TFileEntry = Record
    FileType: Byte;
    FileName: Array [0 .. 15] of Byte;
    FileTrack: Byte;
    FileSector: Byte;
  end;

var
  verbose: boolean; // Flag for fuller info
  AllParameters: boolean; // Flag - all required parameters are provided
  InputError: boolean; // Flag - all input data is correct

  Source1: String; // The name
  S1: File of Byte; // Handle for the actual file

  Source2: String;
  S2: File of Byte;

  MaxTrack: String;

Function TSToSect(T, S: Integer): Integer;
const
  Tracks: Array [1 .. 35] of Integer = (1, 22, 43, 64, 85, 106, 127, 148, 169,
    190, 211, 232, 253, 274, 295, 316, 337, 358, 377, 396, 415, 434, 453, 472,
    491, 509, 527, 545, 563, 581, 599, 616, 633, 650, 667);
begin

  TSToSect := Tracks[T] + S - 1;

end;

procedure CompareFiles(Source1, Source2: String; Source1T, Source1S, Source2T,
  Source2S: Byte);
var
  x1: File of Byte; // Handle for the actual file
  CurrSect1: Integer;
  SectorArray1: Array [0 .. 255] of Byte;

  x2: File of Byte;
  CurrSect2: Integer;
  SectorArray2: Array [0 .. 255] of Byte;

  I: Integer;
  Sourcebyte: Byte;

  FileAddress: Word;
  FileAddressSet: boolean;

  sectlen: Byte;

begin

  if verbose then
    Writeln(Output, '>> First $' + IntToHex(Source1T, 2) + ',$' +
      IntToHex(Source1S, 2));

  FileAddressSet := False;

  AssignFile(x1, Source1);
  Reset(x1);
  AssignFile(x2, Source2);
  Reset(x2);

  Repeat
    if ((Source1T > StrToInt(MaxTrack)) or (Source2T > StrToInt(MaxTrack))) then
    begin
      Writeln(Output,
        '### ERROR: The Track pointer is not valid - skipping the rest of the file');
      Source1T := 0;
    end
    else
    begin
      CurrSect1 := TSToSect(Source1T, Source1S);
      Seek(x1, (CurrSect1 * 256));

      CurrSect2 := TSToSect(Source2T, Source2S);
      Seek(x2, (CurrSect2 * 256));

      // Read the sector and populate the table
      for I := 0 to 255 do
      begin
        Read(x1, Sourcebyte);
        SectorArray1[I] := Sourcebyte;
        Read(x2, Sourcebyte);
        SectorArray2[I] := Sourcebyte;
      end;

      // Print Differences
      if FileAddressSet = False then
      begin
        FileAddress := SectorArray1[2] + (SectorArray1[3] * 256);
        FileAddressSet := True;
      end;

      // Mark cases where lengths are different

      if ((SectorArray1[0] = 0) or (SectorArray2[0] = 0)) then
        if SectorArray1[1] <> SectorArray2[1] then
          Writeln(Output, '### ERROR: File length is different');

      // No read beyond the used bytes of the sector

      if ((SectorArray1[0] = 0) or (SectorArray2[0] = 0)) then
        sectlen := min(SectorArray1[1], SectorArray2[1])
      else
        sectlen := 255;

      // Compare the actual bytes of the sector.

      for I := 2 to sectlen do
      begin
        if SectorArray1[I] <> SectorArray2[I] then
          Writeln(Output, '   ## Difference found at $' +
            IntToHex(FileAddress + I - 2, 4) + ' - 1:$' +
            IntToHex(SectorArray1[I], 2) + ' 2:$' +
            IntToHex(SectorArray2[I], 2));
      end;
      FileAddress := FileAddress + 254;

      // If the sector is the last in the file then Source1T should be 0
      if ((Source1T <> 0) and (Source2T <> 0)) then
      begin
        if verbose then
          Writeln(Output, '>> Next  $' + IntToHex(SectorArray1[0], 2) + ',$' +
            IntToHex(SectorArray1[1], 2));
        Source1T := SectorArray1[0];
        Source1S := SectorArray1[1];
        Source2T := SectorArray2[0];
        Source2S := SectorArray2[1];
      end;
    end;

  Until ((Source1T = 0) or (Source2T = 0));

  if verbose then
    Writeln(Output, 'Closing file' + sLineBreak);

  CloseFile(x1);
  CloseFile(x2);

end;

Function CountDirEntries(D64Name: String): Byte;

var
  CurrSect: Integer;
  FileCnt: Integer;
  NextDirSectorT, NextDirSectorS: Byte;
  Sourcebyte: Byte;
  NumberOfFiles: Byte;
  D64File: File of Byte;
  I: Integer;

begin

  NumberOfFiles := 0;

  AssignFile(D64File, D64Name);
  Reset(D64File);

  NextDirSectorT := 18;
  NextDirSectorS := 1;

  repeat
    CurrSect := TSToSect(NextDirSectorT, NextDirSectorS);
    Seek(D64File, (CurrSect * 256));

    for FileCnt := 0 to 7 do
    begin
      if FileCnt = 0 then
      begin
        Read(D64File, NextDirSectorT); // Track
        Read(D64File, NextDirSectorS); // Sector
      end
      else
      begin
        Read(D64File, Sourcebyte); // Waste if not the first in sector
        Read(D64File, Sourcebyte); // Waste if not the first in sector
      end;

      Read(D64File, Sourcebyte);
      case Sourcebyte of
        $81:
          NumberOfFiles := NumberOfFiles + 1;
        $82:
          NumberOfFiles := NumberOfFiles + 1;
        $C1:
          NumberOfFiles := NumberOfFiles + 1;
        $C2:
          NumberOfFiles := NumberOfFiles + 1
          // ;
          // else
          // Writeln(Output, 'Not counted: $' + IntToHex(Sourcebyte,2))
      end;

      // Track/ Sector / Filename

      for I := 0 to 28 do // Skip bytes
        Read(D64File, Sourcebyte);
    end;
  until (NextDirSectorT = 0);

  if verbose then
    Writeln(Output, 'Number of files: ' + IntToStr(NumberOfFiles));

  CountDirEntries := NumberOfFiles;

end;

procedure FindParameter(SearchString: String; Var ResultString: String);
var
  I: Integer;

begin

  // Writeln(Output, 'Search for parameter: ' + SearchString); // test

  ResultString := '';
  for I := 0 to ParamCount do
  begin
    if pos(SearchString, lowercase(ParamStr(I))) = 2 then
    begin
      ResultString := (ParamStr(I + 1));
      // Writeln(Output, 'Found the parameter: ' + ResultString); // test
      exit;
    end;
  end;

end;

procedure DirToTab(var DirArray: Array of TFileEntry; FileX: String);
var
  CurrSect: Integer;
  FileCnt: Integer;
  Sourcebyte: Byte;

  NextDirSectorT, NextDirSectorS: Byte;
  FileType: Byte;
  FileT, FileS: Byte;
  I: Integer;

  IndexCnt: Integer;

begin
  AssignFile(S1, FileX);
  Reset(S1);

  NextDirSectorT := 18;
  NextDirSectorS := 1;

  IndexCnt := 0;

  repeat
    CurrSect := TSToSect(NextDirSectorT, NextDirSectorS);
    Seek(S1, (CurrSect * 256));

    for FileCnt := 0 to 7 do
    begin

      if verbose then

        Writeln(Output, '>> Next $' + IntToHex(NextDirSectorT, 2) + ',$' +
          IntToHex(NextDirSectorS, 2));

      if FileCnt = 0 then
      begin
        Read(S1, NextDirSectorT); // Track
        Read(S1, NextDirSectorS); // Sector

      end
      else
      begin
        read(S1, Sourcebyte);
        // Waste if not the first in sector
        read(S1, Sourcebyte); // Waste if not the first in sector
      end;

      Read(S1, FileType);
      DirArray[IndexCnt].FileType := FileType;

      Read(S1, FileT);
      DirArray[IndexCnt].FileTrack := FileT;

      Read(S1, FileS);
      DirArray[IndexCnt].FileSector := FileS;

      // FileNameS := '';
      for I := 0 to 15 do
      begin
        Read(S1, Sourcebyte);
        // FileNameS := FileNameS + Char(SourceByte);
        DirArray[IndexCnt].FileName[I] := Sourcebyte;
      end;

      for I := 0 to 10 do // Just waste bytes
        Read(S1, Sourcebyte);

      (* Read(S1, SourceByte); // 15
        Read(S1, SourceByte); // 16
        Read(S1, SourceByte); // 17
        Read(S1, SourceByte); // 18
        Read(S1, SourceByte); // 19
        Read(S1, SourceByte); // 1a
        Read(S1, SourceByte); // 1b
        Read(S1, SourceByte); // 1c
        Read(S1, SourceByte); // 1d
        Read(S1, SourceByte); // 1e
        Read(S1, SourceByte); // 1f
      *)

      IndexCnt := IndexCnt + 1;
    end;

  until NextDirSectorT = 0;

  CloseFile(S1);

end;

Procedure DoCompare(DirArray1, DirArray2: Array of TFileEntry);
var

  Equality: boolean;
  I, J, K: Integer;
  OutPutString: String;

begin
  AssignFile(S1, Source1);
  Reset(S1);
  AssignFile(S2, Source2);
  Reset(S2);

  for I := 0 to Length(DirArray1) do
  begin
    for J := 0 to Length(DirArray2) do
    begin

      Equality := True;

      for K := 0 to 15 do
      begin
        if DirArray1[I].FileName[K] <> DirArray2[J].FileName[K] then
          Equality := False;
      end;

      if Equality then
      begin

        // Print the filename
        SetString(OutPutString, PAnsiChar(@DirArray1[I].FileName), 15);
        Writeln(Output, 'File: ' + OutPutString);

        if verbose then
          Writeln(Output, 'Disk , File ' + IntToStr(I) + ' ($' +
            IntToHex(DirArray1[I].FileTrack, 2) + ' ,$' +
            IntToHex(DirArray1[I].FileSector, 2) + ' and Disk 2, File ' +
            IntToStr(J) + ' $' + IntToHex(DirArray2[J].FileTrack, 2) + ',$' +
            IntToHex(DirArray2[J].FileSector, 2));

        // Execute the compare
        CompareFiles(Source1, Source2, DirArray1[I].FileTrack,
          DirArray1[I].FileSector, DirArray2[J].FileTrack,
          DirArray2[J].FileSector);

      end;

    end;
  end;

  // Writeln(Output,'I and J were ' + IntToStr(I) + ' and ' + IntToStr(J));

  CloseFile(S1);
  CloseFile(S2);

end;

procedure MatchFiles;
var
  FileCnt: Integer;

  Dir1Array: Array Of TFileEntry;
  Dir2Array: Array Of TFileEntry;

begin

  FileCnt := CountDirEntries(Source1);
  if verbose then
    Writeln(Output, 'Number of files: ' + IntToStr(FileCnt));

  // Create the list for D64 - 1
  // Call routine with pointer to table as var to generate table 1

  if verbose then
    Writeln(Output, '>> Generating table for Source1');
  FileCnt := CountDirEntries(Source1);

  SetLength(Dir1Array, FileCnt);
  DirToTab(Dir1Array, Source1);

  // Create the list for D64 - 2
  // Call routine with pointer to table as var to generate table 2

  if verbose then
    Writeln(Output, '>> Generating table for Source2');
  FileCnt := CountDirEntries(Source2);

  SetLength(Dir2Array, FileCnt);
  DirToTab(Dir2Array, Source2);

  // Execute compare between the two tables
  if verbose then
    Writeln(Output, sLineBreak + '>> Doing the compare' + sLineBreak);
  DoCompare(Dir1Array, Dir2Array);

end;


// ####   Main starts here   ####

begin
  AllParameters := True;
  InputError := False;

  try

    Writeln(Output,
      'D64 File Match 1.0 beta 6 - (C) 2016 by Pontus "Bacchus" Berg');
    Writeln(Output, '');

    if (FindCmdLineSwitch('h', True) or FindCmdLineSwitch('?') or
      (ParamCount = 0)) then
    begin
      Writeln(Output,
        'Purpose: Convert a text file to c64 format and screen layout');
      Writeln(Output, 'Usage:   FileMatch /s1 /s2 [/v] [/h] [/?]');
      Writeln(Output, '');
      Writeln(Output, '/s1           Source 1');
      Writeln(Output, '/s2           Source 2');
      Writeln(Output, '/max          Maximal sectors - defaults to 35');

      Writeln(Output, '/h or /?      help (this)');
      Writeln(Output, '/v            verbose output');
      exit;
    end
    else
    begin

      // Flag if verbose output is requester
      If FindCmdLineSwitch('v', True) then
        verbose := True
      else
        verbose := False;

      // Set the source1 file
      If FindCmdLineSwitch('s1', True) then
      begin
        FindParameter('s1', Source1);
        if Fileexists(Source1) = False then
        begin
          AllParameters := False;
          Writeln('ERROR: Source file 1 doesn''t exist');
        end
        else
        begin
          if verbose then
            Writeln(Output, 'Assigning the file: ' + Source1);
        end;
      end;

      // Set the source1 file
      If FindCmdLineSwitch('s2', True) then
      begin
        FindParameter('s2', Source2);
        if Fileexists(Source2) = False then
        begin
          AllParameters := False;
          Writeln('ERROR: Source file 2 doesn''t exist');
        end
        else
        begin
          if verbose then
            Writeln(Output, 'Assigning the file: ' + Source2);
        end;
      end;

      If FindCmdLineSwitch('max', True) then
      begin
        FindParameter('max', MaxTrack);
      end
      else
        MaxTrack := '35';

      // Here all parameters and prerequisites should be validated and the corresponding flags set
      if ((AllParameters = True) AND (InputError = False)) then
      begin
        // Writeln(Output, 'Execute the main program');

        MatchFiles;

      end
      else
        Writeln(Output, 'Error in the input data - fix and run me again');
    end;

    { TODO -oUser -cConsole Main : Insert code here }
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;

end.
